<?php

namespace Drupal\label_link_attributes\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'label_link_attributes_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "label_link_attributes_formatter",
 *   label = @Translation("Label with link attributes"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class LabelLinkAttributesFormatter extends EntityReferenceLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link_label_attributes' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'link_label_attributes' => [
        // phpcs:disable
        '#title' => t('Attributes'),
        // phpcs:enable
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('link_label_attributes'),
      ],
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // phpcs:disable
    // The StringTranslation trait to replace t() isn't used in the base classes.
    // So we leave it here as well.
    $summary[] = $this->getSetting('link_label_attributes') && $this->getSetting('link') ? t('Add attributes to the entity link') : t('No additional attributes');
    // phpcs:enable
    return $summary + parent::settingsSummary();;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $output_as_link = $this->getSetting('link');
    if ($output_as_link) {
      // Convert attributes string to an array.
      $attribute_string = $this->getSetting('link_label_attributes');
      if (!empty($attribute_string)) {
        $dom = Html::load("<foo " . $attribute_string . "/>");
        $attributes = [];

        foreach ($dom->getElementsByTagName('foo') as $element) {
          foreach ($element->attributes as $attr) {
            $attributes[$attr->nodeName] = $attr->nodeValue;
          }
        }

        // Add attributes to the items.
        foreach ($items as &$item) {
          $item->_attributes = $attributes;
        }
      }
    }

    return parent::viewElements($items, $langcode);
  }

}
